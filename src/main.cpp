#include <zelus/render.hpp>

struct object
{
  std::string model;
  std::string texture;
  float x = 0.0f, y = 0.0f, z = 0.0f;
};

int
main ()
{
  std::vector<object> objects;

  {
    object viking_room = {};
    viking_room.model = PKGDATADIR "/models/viking_room.obj";
    viking_room.texture = PKGDATADIR "/textures/viking_room.png";
    viking_room.x = 3.0f;
    objects.push_back (viking_room);
    
    object chalet = {};
    chalet.model = PKGDATADIR "/models/chalet.obj";
    chalet.texture = PKGDATADIR "/textures/chalet.jpg";
    chalet.x = -1.0f;
    objects.push_back (chalet);
  }
  
  zelus::render render;
  for (auto& my_object : objects)
    {
      render.load_model (my_object.model, my_object.texture, my_object.x, my_object.y, my_object.z);
    }
  render.draw ();
}
